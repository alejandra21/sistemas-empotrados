@
@ Sistemas Empotrados
@ El "hola mundo" en la Redwire EconoTAG
@

@
@ Constantes
@

.data

LED_RED_MASK:    .word (1 << (12))

LED_GREEN_MASK:  .word (1 << (13))

S1_OUT_MASK:     .word (1 << (22))

S2_OUT_MASK:     .word (1 << (23))

S1_IN_MASK:      .word (1 << (26))

S2_IN_MASK:      .word (1 << (27))
@ Retardo para el parpadeo
DELAY:           .word       0x00080000

@
@ Punto de entrada
@

        .code 32
        .text
        .global _start
        .type   _start, %function

_start:

        bl       gpio_init

        @ Direcciones de los registros GPIO_DATA_SET1 y GPIO_DATA_RESET1
        ldr     r6, =GPIO_DATA_SET1
        ldr     r7, =GPIO_DATA_RESET1

loop:
		@ Se verifica si se encendio algun boton
		bl		test_buttons


        @ Encendemos el led
        str     r5, [r6]

        @ Pausa corta
        ldr     r0, =DELAY
        ldr 	r0,[r0]
        bl      pause

        @ Apagamos el led
        str     r5, [r7]

		@ Se verifica si se encendio algun boton
        bl		test_buttons

        @ Pausa corta
        ldr     r0, =DELAY
        ldr 	r0,[r0]
        bl      pause

        @ Bucle infinito
        b       loop
        
@
@ Función que produce un retardo
@ r0: iteraciones del retardo
@
.type   pause, %function

pause:
        subs    r0, r0, #1
        bne     pause
        mov     pc, lr

.type   gpio_init, %function

gpio_init:

        @ Configuramos el GPIO44 y 45 para que sea de salida
        ldr     r4, =GPIO_PAD_DIR1
        ldr     r5, =LED_RED_MASK
        ldr     r5, [r5]
        ldr     r3, =LED_GREEN_MASK
        ldr     r3, [r3]
        orr	r3, r3, r5
        str     r3, [r4]


		@ Fijamos un 1 en los pines de salida de los botones
        ldr     r8, =GPIO_DATA_SET0
        ldr	r3, =S2_OUT_MASK
        ldr	r3, [r3]
        ldr     r9, =S1_OUT_MASK
        ldr     r9, [r9]
        orr	r9, r9, r3
        str     r9, [r8]

        mov     pc, lr

.type   test_buttons, %function
test_buttons:

	ldr     r10, =GPIO_DATA0
	ldr     r11, [r10]
	ldr	r3, =LED_GREEN_MASK
	tst	r11, #0x04000000
	ldrne   r5, [r3]
	beq	test_s2

	mov     pc, lr

test_s2:

	ldr	r3, =LED_RED_MASK
	tst	r11, #0x08000000
	ldrne	r5, [r3]
	mov     pc, lr

	@and	r9,r10, #0x04000000
	@and	r11,r10, #0x0c000000
