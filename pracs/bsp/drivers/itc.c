/*
 * Sistemas operativos empotrados
 * Driver para el controlador de interrupciones del MC1322x
 */

#include "system.h"

/*****************************************************************************/
volatile uint32_t const intcntl_irq_enable = (0 << (20));
volatile uint32_t const intcntl_fiq_enable = (0 << (19));
volatile uint32_t const intcntl_irq_disable = (1 << (20));
volatile uint32_t const intcntl_fiq_disable = (1 << (19));

/**
 * Acceso estructurado a los registros de control del ITC del MC1322x
 */
typedef struct
{
	/* ESTA ESTRUCTURA SE DEFINIRÁ EN LA PRÁCTICA 6 */
	uint32_t   intcntl;
	uint32_t   nimask;
	uint32_t   intennum;
	uint32_t   intdisnum;
	uint32_t   intenable;
	uint32_t   inttype;
	uint32_t   reserved[4];
	uint32_t   nivector;
	uint32_t   fivector;
	uint32_t   intsrc;
	uint32_t   intfrc;
	uint32_t   nipend;
	uint32_t   fipend;

} itc_regs_t;

volatile itc_regs_t* const itc_regs = ITC_BASE;
static uint32_t estado_anterior_intenable;
/**
 * Tabla de manejadores de interrupción.
 */
static itc_handler_t itc_handlers[itc_src_max];

/*****************************************************************************/

/**
 * Inicializa el controlador de interrupciones.
 * Deshabilita los bits I y F de la CPU, inicializa la tabla de manejadores a NULL,
 * y habilita el arbitraje de interrupciones Normales y rápidas en el controlador
 * de interupciones.
 */
inline void itc_init ()
{
	uint32_t  habilita_fiq_irq; 

	// Se inicializa la tabla de manejadores a NULL
	int i;
	for (i = 0; i < itc_src_max; ++i)
	{
		itc_handlers[i] = NULL;
	}

	// Desabilita los bits I y F de la CPU

/*	desabilita_firq_irq = intcntl_irq_disable | intcntl_fiq_disable;
	*(itc_regs->intcntl) = desabilita_firq_irq ;*/

	itc_regs->intfrc = 0;

	// Enable pending interrup request to the CPU.
	itc_regs->intenable = 0;

	// Se habilita el arbitraje de interrupciones normales y rapidas en el 
	// controlador de interrupciones.
	habilita_fiq_irq = intcntl_irq_enable | intcntl_fiq_enable;
	itc_regs->intcntl = habilita_fiq_irq;



}

/*****************************************************************************/

/**
 * Deshabilita el envío de peticiones de interrupción a la CPU
 * Permite implementar regiones críticas en modo USER
 */
inline void itc_disable_ints ()
{

	// Se guarda el viejo estado del registro INTENABLE
	estado_anterior_intenable = itc_regs->intenable;

	// Se deshabilitan las interrupciones
	itc_regs->intenable = 0;
}

/*****************************************************************************/

/**
 * Vuelve a habilitar el envío de peticiones de interrupción a la CPU
 * Permite implementar regiones críticas en modo USER
 */
inline void itc_restore_ints ()
{
	// Se restaura el estado del registro INTENABLE antes de
	// deshabilitar interrupciones.

	itc_regs->intenable = estado_anterior_intenable;
}

/*****************************************************************************/

/**
 * Asigna un manejador de interrupción
 * @param src		Identificador de la fuente
 * @param handler	Manejador
 */
inline void itc_set_handler (itc_src_t src, itc_handler_t handler)
{
	itc_handlers[src] = handler;
}

/*****************************************************************************/

/**
 * Asigna una prioridad (normal o fast) a una fuente de interrupción
 * @param src		Identificador de la fuente
 * @param priority	Tipo de prioridad
 */
inline void itc_set_priority (itc_src_t src, itc_priority_t priority)
{
	// Se verifica que solo existe una interrupcion de tipo FIQ
	if ( ( (itc_regs->inttype & 0) == 0) || (src != itc_priority_fast) ){

		uint32_t priority_mask = (priority << (src));
		itc_regs->inttype = itc_regs->inttype | priority_mask;
	}

}

/*****************************************************************************/

/**
 * Habilita las interrupciones de una determinda fuente
 * @param src		Identificador de la fuente

	intennum[0:3]

 */
inline void itc_enable_interrupt (itc_src_t src)
{
	itc_regs->intennum = src;

}

/*****************************************************************************/

/**
 * Deshabilita las interrupciones de una determinda fuente
 * @param src		Identificador de la fuente

	intdisnum[0:3]

 */
inline void itc_disable_interrupt (itc_src_t src)
{
	itc_regs->intdisnum = src;
}

/*****************************************************************************/

/**
 * Fuerza una interrupción con propósitos de depuración
 * @param src		Identificador de la fuente
 */
inline void itc_force_interrupt (itc_src_t src)
{
	uint32_t force_interrupt_mask = (1 << (src));
	itc_regs->intfrc = force_interrupt_mask;
}

/*****************************************************************************/

/**
 * Desfuerza una interrupción con propósitos de depuración
 * @param src		Identificador de la fuente
 */
inline void itc_unforce_interrupt (itc_src_t src)
{
	uint32_t force_interrupt_mask = (0 << (src));
	itc_regs->intfrc = force_interrupt_mask;
}

/*****************************************************************************/

/**
 * Da servicio a la interrupción normal pendiente de más prioridad.
 * En el caso de usar un manejador de excepciones IRQ que permita interrupciones
 * anidadas, debe deshabilitar las IRQ de menor prioridad hasta que se haya
 * completado el servicio de la IRQ para evitar inversiones de prioridad
 */
void itc_service_normal_interrupt ()
{
	/* ESTA FUNCIÓN SE DEFINIRÁ EN LA PRÁCTICA 6 */
	uint32_t highest_pending_irq;
	highest_pending_irq = itc_regs->nivector;
	itc_handlers[highest_pending_irq]();

}

/*****************************************************************************/

/**
 * Da servicio a la interrupción rápida pendiente de más prioridad
 */
void itc_service_fast_interrupt ()
{
	uint32_t highest_pending_fiq;
	highest_pending_fiq = itc_regs->fivector;
	itc_handlers[highest_pending_fiq]();
}

/*void ISR() {

	itc_unforce_interrupt(itc_src_asm);
	*reg_gpio_data_set1_isr = led_green_mask2;
}*/

/*****************************************************************************/
