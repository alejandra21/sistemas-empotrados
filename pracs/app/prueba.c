#define REG_GPIO_PAD_DIR1		((volatile int *) 0x80000004)
#define REG_GPIO_SET1			((volatile int *) 0x8000004C)
#define REG_GPIO_RESET1			((volatile int *) 0x80000054)
#define LED_RED_MASK			(0X00002000)
#define	RETARDO					(0X00000100)

void main(void){

	register int i;
	*REG_GPIO_PAD_DIR1 = LED_RED_MASK;

	while (1) {

		*REG_GPIO_SET1 = LED_RED_MASK;

		for (i = 0; i < RETARDO; ++i);

		*REG_GPIO_RESET1 = LED_RED_MASK;

		for (i = 0; i < RETARDO; ++i);

	}
}