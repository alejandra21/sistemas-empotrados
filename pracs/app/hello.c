/*****************************************************************************/
/*                                                                           */
/* Sistemas Empotrados                                                       */
/* El "hola mundo" en la Redwire EconoTAG en C                               */
/*                                                                           */
/*****************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include "system.h"

/*
 * Constantes relativas a la plataforma
 */

/* Dirección del registro de control de dirección del GPIO32-GPIO63 */
volatile uint32_t * const reg_gpio_pad_dir1    = (uint32_t *) 0x80000004;

/* Dirección del registro de activación de bits del GPIO32-GPIO63 */
volatile uint32_t * const reg_gpio_data_set1   = (uint32_t *) 0x8000004c;

/* Dirección del registro de limpieza de bits del GPIO32-GPIO63 */
volatile uint32_t * const reg_gpio_data_reset1 = (uint32_t *) 0x80000054;

volatile uint32_t * const reg_gpio_pad_dir0  = (uint32_t *) 0x80000000;
volatile uint32_t * const reg_gpio_data0 = (uint32_t *) 0x80000008;
volatile uint32_t * const reg_gpio_set0 = (uint32_t *) 0x80000048;


/* El led rojo está en el GPIO 44 (el bit 12 de los registros GPIO_X_1) */
volatile uint32_t const led_red_mask = (1 << (44-32));
volatile uint32_t const led_green_mask = (1 << (44-31));

/* Mascara para los botones */
volatile uint32_t const s1_out_mask = (1 << (22));
volatile uint32_t const S2_out_mask = (1 << (23));
volatile uint32_t const s1_in_mask  = (1 << (26));
volatile uint32_t const s2_in_mask  = (1 << (27));


#define LED_RED gpio_pin_44
#define LED_GREEN gpio_pin_45
#define S1_OUT gpio_pin_22
#define S2_OUT gpio_pin_23

/*
 * Constantes relativas a la aplicacion
 */
uint32_t const delay = 0x00080000;

uint8_t blink_red_led = 1;
uint8_t blink_green_led = 1;
 
/*****************************************************************************/

/*
 * Inicialización de los pines de E/S
 */
void gpio_init(void)
{
	/* Configuramos el GPIO44 y GPIO45 para que sea de salida */
	gpio_set_pin_dir_output (LED_RED);
	gpio_set_pin_dir_output (LED_GREEN);

	// Fijamos un 1 en los pines de salida de los botones
	gpio_set_pin (S1_OUT);
	gpio_set_pin (S2_OUT);
}

/*****************************************************************************/

/*
 * Enciende los leds indicados en la máscara
 * @param mask Máscara para seleccionar leds
 */
void leds_on (uint32_t   mask)
{
	/* Encendemos los leds indicados en la máscara */
	*reg_gpio_data_set1 = mask;
}

/*****************************************************************************/

/*
 * Apaga los leds indicados en la máscara
 * @param mask Máscara para seleccionar leds
 */
void leds_off (uint32_t  mask)
{
	/* Apagamos los leds indicados en la máscara */
	*reg_gpio_data_reset1 = mask;
}

/*****************************************************************************/

/*
 * Retardo para el parpedeo
 */
void pause(void)
{
	uint32_t i;
	for (i=0 ; i<delay ; i++);
}
/*****************************************************************************/

/*
 * Funcion que verifica si alguno de los botones fue pulsado
 *
 */
uint32_t test_buttons(uint32_t the_leds) {

	uint32_t * contenido_registro_data0;
	uint32_t * contenido_registro_data1;
	gpio_get_pin(S1_OUT,contenido_registro_data0);
	gpio_get_pin(S2_OUT,contenido_registro_data1);

	if (*contenido_registro_data0 > 0){
		the_leds = LED_GREEN;
	}
	else if (*contenido_registro_data1 > 0){
		the_leds = LED_RED;

	}

	return the_leds;	

}

void ledsState(void){


	char caracter;
	caracter = getchar();

	if (caracter=='r'){

		if (blink_red_led == 1){
			blink_red_led = 0;
		}
		else if (blink_red_led == 0){
			blink_red_led = 1;
		}

	}
	else if (caracter=='g') {

		if (blink_green_led == 1){
			blink_green_led = 0;
		}
		else if (blink_green_led == 0){
			blink_green_led = 1;
		}

	}
	
	
}

/*****************************************************************************/

/*
 * Máscara del led que se hará parpadear
 */

/*
 * Programa principal
 */
int main ()
{

		// Se inicializa la UART
		uart_init (uart_1, 115200, "hola");
		uart_set_receive_callback (uart_1,*ledsState);

		// Se inicializan los leds
		gpio_init();
		
		printf("Introduzca r o g: \r\n");

		while (1)
		{

			if (blink_green_led == 1 && blink_red_led == 1){

				gpio_set_pin (LED_RED);
				gpio_set_pin (LED_GREEN);
				pause();
				gpio_clear_pin (LED_RED);
				gpio_clear_pin (LED_GREEN);
				pause();

			}
			else if (blink_green_led == 1 && blink_red_led == 0){

				gpio_set_pin (LED_GREEN);
				gpio_clear_pin (LED_RED);
				pause();
				gpio_clear_pin (LED_GREEN);
				pause();

			}
			else if (blink_green_led == 0 && blink_red_led == 1){
				
				gpio_set_pin (LED_RED);
				gpio_clear_pin (LED_GREEN);
				pause();
				gpio_clear_pin (LED_RED);
				pause();

			}
			else{

				gpio_clear_pin (LED_GREEN);
				gpio_clear_pin (LED_RED);
				pause();

			}
			
		}

	    return 0;
}